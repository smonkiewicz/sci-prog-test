%Scientific Programmer CSIRO - Coding test
%Contour plots

%clear any variables saved
clear time1 lat1 long1 hs1 dir1 u1 v1 windspd1 i j F

%Load from data file
time1=ncread('ww3.aus_4m.201606.nc','time');
lat1=ncread('ww3.aus_4m.201606.nc','latitude');
long1=ncread('ww3.aus_4m.201606.nc','longitude');
hs1=ncread('ww3.aus_4m.201606.nc','hs'); %sig wave height
dir1=ncread('ww3.aus_4m.201606.nc','dp'); %peak wave direction
u1=ncread('ww3.aus_4m.201606.nc','uwnd'); %eastward wind
v1=ncread('ww3.aus_4m.201606.nc','vwnd'); %northward wind
windspd1=sqrt(u1.^2 + v1.^2); %wind speed magnitude

%Plot sig wave height for each day at 12pm
figure
subplot(2,3,1)
contourf(long1,lat1,transpose(hs1(:,:,13)),'linestyle','none') %Contour plot for sig wave height
axis([149 156 -37 -28]) %manually set axes for plot
title('Sig Wave Height - June 4 12pm','fontweight','bold')
xlabel('Longitude (degrees)','fontweight','bold')
ylabel('Latitude (degrees)','fontweight','bold')
text(159,-31,'Wave Height (m)','rotation',270,'fontweight','bold')
colorbar
caxis manual
caxis([0 10]) %manually set colorbar axes
subplot(2,3,2)
contourf(long1,lat1,transpose(hs1(:,:,37)),'linestyle','none')
axis([149 156 -37 -28])
title('Sig Wave Height - June 5 12pm','fontweight','bold')
xlabel('Longitude (degrees)','fontweight','bold')
text(159,-31,'Wave Height (m)','rotation',270,'fontweight','bold')
colorbar
caxis manual
caxis([0 10])
subplot(2,3,3)
contourf(long1,lat1,transpose(hs1(:,:,61)),'linestyle','none')
axis([149 156 -37 -28])
title('Sig Wave Height - June 6 12pm','fontweight','bold')
xlabel('Longitude (degrees)','fontweight','bold')
text(159,-31,'Wave Height (m)','rotation',270,'fontweight','bold')
colorbar
caxis manual
caxis([0 10])

%Plot wind speed for each day as above 
subplot(2,3,4)
contourf(long1,lat1,transpose(windspd1(:,:,13)),'linestyle','none')
axis([149 156 -37 -28])
title('Wind Speed - June 4 12pm','fontweight','bold')
xlabel('Longitude (degrees)','fontweight','bold')
ylabel('Latitude (degrees)','fontweight','bold')
text(159,-31,'Wind Speed (m/s)','rotation',270,'fontweight','bold')
colorbar
caxis manual
caxis([0 25])
subplot(2,3,5)
contourf(long1,lat1,transpose(windspd1(:,:,37)),'linestyle','none')
axis([149 156 -37 -28])
title('Wind Speed - June 5 12pm','fontweight','bold')
xlabel('Longitude (degrees)','fontweight','bold')
text(159,-31,'Wind Speed (m/s)','rotation',270,'fontweight','bold')
colorbar
caxis manual
caxis([0 25])
subplot(2,3,6)
contourf(long1,lat1,transpose(windspd1(:,:,61)),'linestyle','none')
axis([149 156 -37 -28])
title('Wind Speed - June 6 12pm','fontweight','bold')
xlabel('Longitude (degrees)','fontweight','bold')
text(159,-31,'Wind Speed (m/s)','rotation',270,'fontweight','bold')
colorbar
caxis manual
caxis([0 25])

%Animation for Sig wave height
figure %standard plotting parameters
axis([149 156 -37 -28])
xlabel('Longitude (degrees)','fontweight','bold')
ylabel('Latitude (degrees)','fontweight','bold')
text(157.5,-31,'Wave Height (m)','rotation',270,'fontweight','bold')
colorbar
caxis manual
caxis([0 10])
hold on;
for i=1:length(time1) %loop through each hour and plot contour
    clear title %clear title each loop to update hour
    title(sprintf('Significant Wave Height at hour %d',i),'fontweight','bold');
    contourf(long1,lat1,transpose(hs1(:,:,i)),'linestyle','none');
    F(i)=getframe(gcf); %Allows me to save each frame for the video file
    drawnow %draws plot at specified 'i' time point
end
video=VideoWriter('SigWaveHeight.avi','MPEG-4'); %Writes to video file
video.FrameRate=3; %Sets frame rate at 2fps
open(video)
writeVideo(video,F);
close(video)


% Unused quiver plot   
% figure
% axis([149 159 -37 -28])
% colorbar
% title('Wind speed off coast of NSW on June 4 12pm','fontsize',15,'fontweight','bold')
% xlabel('Longitude (degrees)','fontweight','bold')
% ylabel('Latitude (degrees)','fontweight','bold')
% text(161,-31,'Wind speed (m/s)','rotation',270,'fontweight','bold')
% caxis manual
% caxis([0 25]);
% quiver(long1,lat1,transpose(u1(:,:,39)),transpose(v1(:,:,39)))
