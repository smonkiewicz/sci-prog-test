%Scientific Programmer CSIRO - Coding test

%clear any variables saved
clear time1 lat1 long1 hs1 dir1 u1 v1 windspd1 i j

%Could load the below array data using OPeNDAP, but not grid data (eg. hs, dp), so unused
%time1=ncread('http://data-cbr.csiro.au/thredds/dodsC/catch_all/CMAR_CAWCR-Wave_archive/CAWCR_Wave_Hindcast_aggregate/gridded/ww3.aus_4m.201606.nc','time');
%long1=ncread('http://data-cbr.csiro.au/thredds/dodsC/catch_all/CMAR_CAWCR-Wave_archive/CAWCR_Wave_Hindcast_aggregate/gridded/ww3.aus_4m.201606.nc','longitude');
%lat1=ncread('http://data-cbr.csiro.au/thredds/dodsC/catch_all/CMAR_CAWCR-Wave_archive/CAWCR_Wave_Hindcast_aggregate/gridded/ww3.aus_4m.201606.nc','latitude');

%Load from data file
time1=ncread('ww3.aus_4m.201606.nc','time');
lat1=ncread('ww3.aus_4m.201606.nc','latitude');
long1=ncread('ww3.aus_4m.201606.nc','longitude');
hs1=ncread('ww3.aus_4m.201606.nc','hs'); %sig wave height
dir1=ncread('ww3.aus_4m.201606.nc','dp'); %peak wave direction
u1=ncread('ww3.aus_4m.201606.nc','uwnd'); %eastward wind
v1=ncread('ww3.aus_4m.201606.nc','vwnd'); %northward wind
windspd1=sqrt(u1.^2 + v1.^2); %wind speed magnitude

%Find maximum sig wave height over time series
for i=1:length(time1)
    hsmax(i)=max(max(hs1(:,:,i)));
end
%plot max sig wave height against hours from June 4 12am
figure
plot(0:1:71,hsmax) 
xlabel('Hours from June 4th 12am','fontweight','bold')
ylabel('Wave Height (m)','fontweight','bold')
title('Maximum Significant Wave Height from June 4th - 6th','fontweight','bold','fontsize',15)
    
%Find mean peak wave direction over time series
for j=1:length(time1)
    dirmean(j)=mean(mean(dir1(:,:,j),'omitnan'),'omitnan');
end
%plot mean peak wave direction against hours from June 4 12am
figure
plot(0:1:71,dirmean) 
xlabel('Hours from June 4th 12am','fontweight','bold')
ylabel('Mean Wave direction (degrees)','fontweight','bold')
title('Mean Peak Wave Direction from June 4th - 6th','fontweight','bold','fontsize',15)

%Find maximum wind speed over time series
for k=1:length(time1)
    windspdmax(k)=max(max(windspd1(:,:,k)));
end
%plot max wind speed against hours from June 4 12am
figure
plot(0:1:71,windspdmax) 
xlabel('Hours from June 4th 12am','fontweight','bold')
ylabel('Wind speed (m/s)','fontweight','bold')
title('Maximum Wind Speed from June 4th - 6th','fontweight','bold','fontsize',15)
